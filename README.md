# regex_router

Material router with regex path support.

## Usage

Just define `router` instance and pass `router.generateRoute` to `MaterialApp` or any other `Navigator`.

```dart
final router = RegexRouter.create({
    "/": (context, _) => SamplePage(title: "BLoC showcase"),
    "/post/:id": (context, args) => PostPage(
        id: args["id"]), // Access path arguments with their names.
    "/post/:id/attachment/:attachmentId": (context, args) => PostAttachmentPage(
        id: args["id"],
        attachmentId: args["attachmentId"],
        body: args.body as Map<String, String>), // Access "object" arguments from `NavigatorState.pushNamed`.
    "/post/:id/submit(\\?success=(?<success>true|false))?" // add optional query parameters
});

return MaterialApp(
    onGenerateRoute: router.generateRoute,
    initialRoute: "/",
    title: 'Flutter Demo',
    theme: ThemeData(primarySwatch: Colors.blue),
);

// Somewhere inside `MaterialApp` container:

Navigator.of(context).pushNamed("/post/7"); // Navigate to path with argument.
Navigator.of(context).pushNamed("/post/7/attachment/8", argument: {"payload": "data"}); // Navigate to path with argument and body.
Navigator.of(context).pushNamed("/post/7/submit?success=true"); // Navigate to path with argument and body.
```

## Samples

You can experiment with a sample application hosted here: https://gitlab.com/marcin.jelenski/bloc-showcase

Also `test` directory contains all router use cases.