## [2.0.2] - 4.01.2021

* Fix documentation.

## [2.0.1] - 4.01.2021

* Fix trailing slash matching for queries.

## [2.0.0] - 4.04.2021

* Null safety.

## [1.0.5] - 8.12.2020

* Update repository url.

## [1.0.4] - 8.12.2020

* Update repository url.

## [1.0.3] - 15.07.2020

* Custom characters - bugfix.

## [1.0.2] - 15.07.2020

* Recognize more characters for arguments with colon (':arg').

## [1.0.1] - 10.05.2020

* MIT License.

## [1.0.0] - 10.05.2020

* Promote to 1.0.0.

## [0.1.0+2] - 2.12.2019

* Add example.
  
## [0.1.0+1] - 2.12.2019

* Promote to 0.1.0.

## [0.0.2] - 1.12.2019

* Fix documentation typos.
 
## [0.0.1] - 1.12.2019

* Initial release.
