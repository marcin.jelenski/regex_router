import 'package:flutter/material.dart';
import 'package:regex_router/regex_router.dart';

void main() {
  // Create router instance
  final router = RegexRouter.create({
    "/": (context, _) => HomePage(),
    "/second/:id/": (context, args) => AnotherPage(id: args["id"]!),
    "/withBody": (context, args) => PageWithBody(body: args.body!),
  });

  // Run material app with router
  runApp(MaterialApp(
    onGenerateRoute: router.generateRoute,
    initialRoute: "/",
    theme: ThemeData(
      primarySwatch: Colors.blue,
    ),
  ));

  // ... or use it with any Navigator
  Navigator(
    onGenerateRoute: router.generateRoute,
    initialRoute: "/",
  );
}

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        MaterialButton(
          onPressed: () => Navigator.of(context).pushNamed("/second/7"),
          child: Text("Second"),
        ),
        MaterialButton(
          onPressed: () => Navigator.of(context)
              .pushNamed("/withBody", arguments: {"key": "value"}),
          child: Text("Second"),
        ),
      ],
    );
  }
}

class AnotherPage extends StatelessWidget {
  final String id;

  const AnotherPage({Key? key, required this.id}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text("Id: $id");
  }
}

class PageWithBody extends StatelessWidget {
  final Object body;

  const PageWithBody({Key? key, required this.body}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text("Body: $body");
  }
}
