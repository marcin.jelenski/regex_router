import 'package:flutter/material.dart';
import 'package:mockito/mockito.dart';
import 'package:regex_router/regex_router.dart';
import 'package:test/test.dart';

void main() {
  group("RegexRouter", () {
    test("should make a route by name", () {
      // Given
      final widgetBuilder = _WidgetBuilder();
      final router = RegexRouter.create({
        "/first": (context, args) => widgetBuilder.buildWidget(args),
      });

      // When
      final result = router.generateRoute(RouteSettings(name: "/first"))!;

      // Then
      expect(result, isNotNull);
      expect(result, isA<MaterialPageRoute>());

      (result as MaterialPageRoute).builder(_BuildContextMock());
      expect(widgetBuilder.lastArgs, RouteArgs({}));
    });

    test("should make a route with colon arguments", () {
      // Given
      final widgetBuilder = _WidgetBuilder();
      final router = RegexRouter.create({
        "/second/:arg1/:arg2": (context, args) =>
            widgetBuilder.buildWidget(args),
      });

      // When
      final result = router.generateRoute(RouteSettings(name: "/second/a/b"));

      // Then
      expect(result, isNotNull);
      expect(result, isA<MaterialPageRoute>());

      (result as MaterialPageRoute).builder(_BuildContextMock());
      expect(widgetBuilder.lastArgs, RouteArgs({"arg1": "a", "arg2": "b"}));
    });

    test("should make a route with query arguments", () {
      // Given
      final widgetBuilder = _WidgetBuilder();
      final router = RegexRouter.create({
        "/second/:arg1/:arg2\\?success=(true|false)": (context, args) =>
            widgetBuilder.buildWidget(args),
      });

      // When
      final result =
          router.generateRoute(RouteSettings(name: "/second/a/b?success=true"));

      // Then
      expect(result, isNotNull);
      expect(result, isA<MaterialPageRoute>());

      (result as MaterialPageRoute).builder(_BuildContextMock());
      expect(widgetBuilder.lastArgs, RouteArgs({"arg1": "a", "arg2": "b"}));
    });

    test("should make a route with different query arguments", () {
      // Given
      final widgetBuilder = _WidgetBuilder();
      final router = RegexRouter.create({
        "/second/:arg1/:arg2/\\?success=true": (context, args) =>
            widgetBuilder.buildWidget(args),
        "/second/:arg1/:arg2/\\?success=false": (context, args) =>
            widgetBuilder.buildWidget(args),
      });

      // When
      final result = router
          .generateRoute(RouteSettings(name: "/second/a/b/?success=true"))!;

      // Then
      expect(result, isNotNull);
      expect(result, isA<MaterialPageRoute>());

      (result as MaterialPageRoute).builder(_BuildContextMock());
      expect(widgetBuilder.lastArgs, RouteArgs({"arg1": "a", "arg2": "b"}));
    });

    test("should make a route with different query arguments retrieval", () {
      // Given
      final widgetBuilder = _WidgetBuilder();
      final router = RegexRouter.create({
        "/second/:arg1/:arg2/(\\?success=(?<success>true|false))":
            (context, args) => widgetBuilder.buildWidget(args),
      });

      // When
      final result = router
          .generateRoute(RouteSettings(name: "/second/a/b/?success=true"))!;

      // Then
      expect(result, isNotNull);
      expect(result, isA<MaterialPageRoute>());

      (result as MaterialPageRoute).builder(_BuildContextMock());
      expect(
          widgetBuilder.lastArgs,
          RouteArgs({
            "arg1": "a",
            "arg2": "b",
            "success": "true",
          }));

      // When
      final result2 = router
          .generateRoute(RouteSettings(name: "/second/a/b/?success=false"));

      // Then
      expect(result2, isNotNull);
      expect(result2, isA<MaterialPageRoute>());

      (result2 as MaterialPageRoute).builder(_BuildContextMock());
      expect(
          widgetBuilder.lastArgs,
          RouteArgs({
            "arg1": "a",
            "arg2": "b",
            "success": "false",
          }));
    });

    test("should make a route with optional query arguments retrieval", () {
      // Given
      final widgetBuilder = _WidgetBuilder();
      final router = RegexRouter.create({
        "/second/:arg1/:arg2(\\?success=(?<success>true|false))?":
            (context, args) => widgetBuilder.buildWidget(args),
      });

      // When
      final result = router.generateRoute(RouteSettings(name: "/second/a/b"));

      // Then
      expect(result, isNotNull);
      expect(result, isA<MaterialPageRoute>());

      (result as MaterialPageRoute).builder(_BuildContextMock());
      expect(
        widgetBuilder.lastArgs!.pathArgs,
        {
          "arg1": "a",
          "arg2": "b",
          "success": null,
        },
      );

      // When
      final result2 =
          router.generateRoute(RouteSettings(name: "/second/a/b?success=true"));

      // Then
      expect(result2, isNotNull);
      expect(result2, isA<MaterialPageRoute>());

      (result2 as MaterialPageRoute).builder(_BuildContextMock());
      expect(
        widgetBuilder.lastArgs!.pathArgs,
        {
          "arg1": "a",
          "arg2": "b",
          "success": "true",
        },
      );
    });

    test("should make a route with regex arguments", () {
      // Given
      final widgetBuilder = _WidgetBuilder();
      final router = RegexRouter.create({
        "/second/(?<arg1>\\d+)/:arg2/:arg3/:arg4": (context, args) =>
            widgetBuilder.buildWidget(args),
      });

      // When
      final result = router.generateRoute(
          RouteSettings(name: "/second/1/b/-.:;+*^%\$@!/10.5,20.5"));

      // Then
      expect(result, isNotNull);
      expect(result, isA<MaterialPageRoute>());

      (result as MaterialPageRoute).builder(_BuildContextMock());
      expect(
          widgetBuilder.lastArgs,
          RouteArgs({
            "arg1": "1",
            "arg2": "b",
            "arg3": "-.:;+*^%\$@!",
            "arg4": "10.5,20.5",
          }));
    });

    test("should make a route with regex arguments and body arguments", () {
      // Given
      final widgetBuilder = _WidgetBuilder();
      final router = RegexRouter.create({
        "/second/(?<arg1>\\d+)/:arg2": (context, args) =>
            widgetBuilder.buildWidget(args),
      });
      final additionalArgument = "hello";

      // When
      final result = router.generateRoute(RouteSettings(
        name: "/second/1/b",
        arguments: additionalArgument,
      ));

      // Then
      expect(result, isNotNull);
      expect(result, isA<MaterialPageRoute>());

      final materialPageRoute = (result as MaterialPageRoute);
      materialPageRoute.builder(_BuildContextMock());
      expect(widgetBuilder.lastArgs,
          RouteArgs({"arg1": "1", "arg2": "b"}, additionalArgument));
    });

    test("should return null when route is not found", () {
      // Given
      final widgetBuilder = _WidgetBuilder();
      final router = RegexRouter.create({
        "/": (context, args) => widgetBuilder.buildWidget(args),
      });

      // When
      final result = router.generateRoute(RouteSettings(name: "/xyz"));

      // Then
      expect(result, isNull);
    });

    test("should return null when route is empty", () {
      // Given
      final router = RegexRouter.create({});

      // When
      final result = router.generateRoute(RouteSettings(name: "/xyz"));

      // Then
      expect(result, isNull);
    });

    test("should return null when arguments count doesn't match", () {
      // Given
      final widgetBuilder = _WidgetBuilder();
      final router = RegexRouter.create({
        "/:arg1/:arg2": (context, args) => widgetBuilder.buildWidget(args),
      });

      // When
      final result = router.generateRoute(RouteSettings(name: "/a"));

      // Then
      expect(result, isNull);
    });

    test("should return null when argument type doesn't match", () {
      // Given
      final widgetBuilder = _WidgetBuilder();
      final router = RegexRouter.create({
        "/(?<arg1>\\d+)/:arg2": (context, args) =>
            widgetBuilder.buildWidget(args),
      });

      // When
      final result = router.generateRoute(RouteSettings(name: "/a/b"));

      // Then
      expect(result, isNull);
    });
  });
}

class _WidgetBuilder {
  RouteArgs? lastArgs;

  Widget buildWidget(RouteArgs args) {
    lastArgs = args;
    return Container();
  }
}

class _BuildContextMock extends Mock implements BuildContext {}
